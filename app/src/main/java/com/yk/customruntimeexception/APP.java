package com.yk.customruntimeexception;

import android.app.Application;
import android.os.Environment;

import java.io.File;

/**
 * Created by Think on 2018/6/28.
 */

public class APP extends Application {
    public static final String URL = "http://192.168.0.197:8080/UploadFileServer/servlet/UploadHandleServlet";
    public static final String CRASH_FILE_NAME="CRASH_FILE_NAME";
    //log路径
    private static final String LOG_PATH= Environment
            .getExternalStorageDirectory().getPath() + File.separator + "Live" + File.separator
            + "log" + File.separator;

    @Override
    public void onCreate() {
        super.onCreate();
        CrashHandler.getInstance().init(this,LOG_PATH);
    }
}
