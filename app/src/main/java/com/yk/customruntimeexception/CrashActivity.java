package com.yk.customruntimeexception;

import android.app.ProgressDialog;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.yk.customruntimeexception.http.FileUploadObserver;
import com.yk.customruntimeexception.http.RetrofitClient;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import okhttp3.ResponseBody;

import static com.yk.customruntimeexception.APP.CRASH_FILE_NAME;
import static com.yk.customruntimeexception.APP.URL;

public class CrashActivity extends AppCompatActivity {


    Button mBtnCrash;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        mBtnCrash=findViewById(R.id.btn_crash);
        mBtnCrash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                throw new RuntimeException(toUtf8("出现异常了"));
            }
        });

        dialog = new ProgressDialog(CrashActivity.this);
        dialog.setMax(100);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setMessage("上传文件中");

        if (!TextUtils.isEmpty(PreferencesUtils.getString(this,CRASH_FILE_NAME))){
            upload(new File(PreferencesUtils.getString(this,CRASH_FILE_NAME)));
        }
    }

    public static String toUtf8(String str) {
        String result = null;
        try {
            result = new String(str.getBytes("UTF-8"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 封装后的单文件上传方法
     */
    public void upload(File file) {
        dialog.show();
        Log.e("File",file.getAbsolutePath());
        RetrofitClient
                .getInstance()
                .upLoadFile(URL, file, new FileUploadObserver<ResponseBody>() {
                    @Override
                    public void onUpLoadSuccess(ResponseBody responseBody) {
                        Toast.makeText(CrashActivity.this, "上传成功", Toast.LENGTH_SHORT).show();
                        try {
                            Log.e("上传进度",responseBody.string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }

                    @Override
                    public void onUpLoadFail(Throwable e) {
                        Toast.makeText(CrashActivity.this, "上传失败"+e.getMessage(), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                    @Override
                    public void onProgress(int progress) {
                        dialog.setProgress(progress);
                    }
                });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!TextUtils.isEmpty(PreferencesUtils.getString(this,CRASH_FILE_NAME))){
            PreferencesUtils.remove(this,CRASH_FILE_NAME);
        }
    }
}